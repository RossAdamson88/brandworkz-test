import { BaseSyntheticEvent, useEffect, useState } from 'react';
import './App.scss';
import CurrentWeather from './components/currentWether/CurrentWeather'
import ForecastWether from './components/ForecastWether/ForecastWether';
import { CurrentWeatherType, ForcastWeatherType } from './types';

function App() {
  const apiKey = '075da84531msh8130ca3e810cdb3p125d52jsnde3de05278db';
  const cities = [
    {
      name: 'London',
      country: 'UK',
    },
    {
      name: 'New York',
      country: 'US',
    },
    {
      name: 'Mumbai',
      country: 'IN',
    },
    {
      name: 'Sydney',
      country: 'AU',
    },
    {
      name: 'Tokyo',
      country: 'JP',
    },
  ]
  const [currentCity, setCurrentCity] = useState(cities[0]);
  const [currentWeather, setCurrentWeather] = useState<CurrentWeatherType>();
  const [forcastWeather, setForcastWeather] = useState<ForcastWeatherType>();
  const [weatherType, setWeatherType] = useState('current');


  useEffect(() => {
    const getCurrentWeather = () => {
      const apiUrl = `https://weatherbit-v1-mashape.p.rapidapi.com/current?city=${currentCity.name}&country=${currentCity.country}`;
      fetch(apiUrl, {
        "method": "GET",
        "headers": {
          "x-rapidapi-key": apiKey,
          "x-rapidapi-host": "weatherbit-v1-mashape.p.rapidapi.com"
        }
      })
        .then(response => response.json())
        .then((data) => {
          setCurrentWeather(data);
        });
    }

    const getForcastWeather = () => {
      const apiUrl = `https://weatherbit-v1-mashape.p.rapidapi.com/forecast/daily?city=${currentCity.name}&country=${currentCity.country}`;
      fetch(apiUrl, {
        "method": "GET",
        "headers": {
          "x-rapidapi-key": apiKey,
          "x-rapidapi-host": "weatherbit-v1-mashape.p.rapidapi.com"
        }
      })
        .then(response => response.json())
        .then((data) => {
          setForcastWeather(data);
        });
    }

    getCurrentWeather();
    getForcastWeather();
  }, [currentCity])

  const handleChange = (event: BaseSyntheticEvent) => {
    setCurrentCity(cities[event.target.value])
  }

  const selectWeatherType = (type: string) => {
    setWeatherType(type)
  }

  return (
    <div className="App">
      <header className="header">
        <nav className="navigation">
          <button className={'navigation-link ' + (weatherType === 'current' ? 'active' : '')} onClick={() => selectWeatherType('current')}>
            Current Weather
          </button>
          <button className={'navigation-link ' + (weatherType === 'forecast' ? 'active' : '')}  onClick={() => selectWeatherType('forecast')}>
            16 Day Weather Forecast
          </button>
        </nav>
        <div className="setting">
          Select a City: 
          <select onChange={handleChange} className="select">
            {cities.map((city, index) => (
              <option key={city.name} value={index}>{city.name}</option>
            ))}
          </select>
        </div>
      </header>
      {weatherType === 'current' && (
      <div>
        {currentWeather && currentWeather.data ? <CurrentWeather currentWeather={currentWeather} ></CurrentWeather> : null}
      </div>
      )}
      {weatherType === 'forecast' && (
      <div>
        {forcastWeather && forcastWeather.data ? <ForecastWether forcastWeather={forcastWeather} currentCity={currentCity} ></ForecastWether> : null}
      </div>
      )}
    </div>
  );
}

export default App;
