import './CurrentWeather.scss';

function CurrentWeather({currentWeather}) {
  const imagePath = `https://www.weatherbit.io/static/img/icons/${currentWeather.data[0].weather.icon}.png`
  
  
  return (
    <div className="current-weather">
      <div>
      <img src={imagePath} alt={currentWeather.data[0].weather.description} />
      </div>
      <h1>{currentWeather.data[0].city_name}</h1>
      <h2>Temp: {currentWeather.data[0].temp}&#8451;</h2>
      <h3>Feels Like: {currentWeather.data[0].app_temp}&#8451;</h3>
     <h4>{currentWeather.data[0].weather.description}</h4>
    </div>
  );
}

export default CurrentWeather;
