import { useEffect, useState } from 'react';
import './ForecastWether.scss';

function ForecastWether({ forcastWeather, currentCity }) {
  const days = ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"];
  const months = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];
  const forcastDays = forcastWeather.data;
  const [filteredForcastDays, setFilteredForcastDays] = useState(forcastDays);
  const [minVal, setMinVal] = useState(-100);
  const [maxVal, setMaxVal] = useState(100);

  const handleMinChange = (event) => {
    setMinVal(event.target.value);
  }

  const handleMaxChange = (event) => {
    setMaxVal(event.target.value);
  }

  useEffect(() => {
    const newForcastDays = forcastDays;
    setFilteredForcastDays(newForcastDays.filter(day => day.app_min_temp >= minVal && day.app_max_temp <= maxVal));
  }, [minVal, maxVal, forcastDays])

  useEffect(() => {
    setMinVal(-100);
    setMaxVal(100);
  }, [currentCity])

  return (
    <div className="forcast-weather">
      <div className="forcast-filter">
        <p>Filter: </p>
        <div className="forcast-filter-inputs">
          <label>
            Min Temp (&#8451;):
            <input type="number"
              onChange={handleMinChange}
              value={minVal}
            />
          </label>
          <label>
            Max Temp (&#8451;):
            <input type="number"
              onChange={handleMaxChange}
              value={maxVal}
            />
          </label>
        </div>
      </div>
      <div className="forcast-weather-items">
        {filteredForcastDays.map(day => {
          const date = new Date(day.valid_date);
          const imagePath = `https://www.weatherbit.io/static/img/icons/${day.weather.icon}.png`
          return (
            <div className="forcast-weather-item" key={day.valid_date}>
              <p className="forcast-weather-item-day" >{days[date.getDay()]}</p>
              <p className="forcast-weather-item-date" >{date.getDate()} {months[date.getMonth()]}</p>
              <img width="100" src={imagePath} alt={day.weather.description} />
              <div className="forcast-weather-temp"><span>Min Temp: </span><strong>{day.app_min_temp}&#8451;</strong></div>
              <div className="forcast-weather-temp"><span>Max Temp: </span><strong>{day.app_max_temp}&#8451;</strong></div>
            </div>
          )
        })}
      </div>
    </div>
  );
}

export default ForecastWether;
